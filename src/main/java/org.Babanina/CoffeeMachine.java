package org.Babanina;

public interface CoffeeMachine {
  void makeDrink(Drink drink);
}
