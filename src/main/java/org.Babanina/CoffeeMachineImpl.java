package org.Babanina;

public class CoffeeMachineImpl implements CoffeeMachine{

  @Override
  public void makeDrink(Drink drink) {
    switch (drink){
      case ESPRESSO:
        printOperations(Drink.ESPRESSO);
        System.out.println("Напиток " + Drink.ESPRESSO + " готов!");
        break;
      case CHOCOLATE:
        printOperations(Drink.CHOCOLATE);
        System.out.println("Напиток " + Drink.CHOCOLATE + " готов!");
        break;
      case BOLLING_WATER:
        printOperations(Drink.BOLLING_WATER);
        System.out.println("Напиток " + Drink.BOLLING_WATER + " готов!");
        break;
      case CAPPUCCINO:
        printOperations(Drink.CAPPUCCINO);
        System.out.println("Напиток " + Drink.CAPPUCCINO + " готов!");
    }
  }

  private void printOperations(Drink drink){
    for(Operations o: drink.getOperations()){
      System.out.println(o.getPhrase());
    }
  }
}
