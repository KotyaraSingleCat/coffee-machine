package org.Babanina;

public enum Drink {
  BOLLING_WATER(new Operations[] { Operations.ADD_COFFEE, Operations.BOIL_WATER } ),
  ESPRESSO(new Operations[] { Operations.ADD_COFFEE, Operations.BOIL_WATER }),
  CHOCOLATE(new Operations[] { Operations.ADD_CHOCOLATE, Operations.BOIL_WATER}),
  CAPPUCCINO(new Operations[] { Operations.ADD_COFFEE, Operations.BOIL_WATER, Operations.ADD_MILK });

  private Operations[] operations;

  Drink(Operations[] operations) {
    this.operations = operations;
  }

  public Operations[] getOperations() {
    return operations;
  }
}
