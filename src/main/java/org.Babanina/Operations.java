package org.Babanina;

public enum Operations {
  ADD_COFFEE("Добавляю кофе!"),
  ADD_MILK("Добавляю молоко!"),
  ADD_CHOCOLATE("Добавляю шоколад!"),
  BOIL_WATER("Кипячу воду!");

  private String phrase;

  Operations(String phrase) {
    this.phrase = phrase;
  }

  public String getPhrase() {
    return phrase;
  }
}
